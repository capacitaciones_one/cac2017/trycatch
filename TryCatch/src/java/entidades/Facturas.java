package entidades;

public class Facturas {
    private String tipo;
    private int precio;

    public Facturas(String tipo, int precio) {
        this.tipo = tipo;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Facturas{" + "tipo=" + tipo + ", precio=" + precio + '}';
    }
    
    

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
}
