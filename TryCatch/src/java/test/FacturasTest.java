package test;

import entidades.Facturas;
import java.util.ArrayList;

public class FacturasTest {

    public static void main(String[] args) {
        System.out.println("[...]");
        Facturas miFactura1 = new Facturas("Medialuna", 15);
        Facturas miFactura2 = new Facturas("Cañoncito", 17);
        Facturas miFactura3 = new Facturas("Pancito", 18);
        Facturas miFactura4 = new Facturas("Palmerita", 14);

        ArrayList<Facturas> listadoFacturas = new ArrayList();
        listadoFacturas.add(miFactura1);
        listadoFacturas.add(miFactura2);
        listadoFacturas.add(miFactura3);
        listadoFacturas.add(miFactura4);
        //listadoFacturas.add(13);

        System.out.println(listadoFacturas);

        try {
            for (int i = 0; i < 5; i++) {
                //Object get = listadoFacturas.get(i);
                String nombreFactura = listadoFacturas.get(i).getTipo();
                System.out.println(nombreFactura);
            }
        } catch (Exception ex) {
            System.out.println("El for esta mal programado!! " + ex.getMessage());
        }

        System.out.println("[OK]");
    }
}
